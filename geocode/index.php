<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Google Maps Geocoding</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<?php
if($_POST){
    function geocode($address){
        
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyDVydhMjuw6sW5O3rrLpNXqs2i1KF9GTtw";
     
        // get the json response
        $resp_json = file_get_contents($url);
        // decode the json
        $resp = json_decode($resp_json, true);
 
        // response status will be 'OK', if able to geocode given address 
        if($resp['status']=='OK'){
           
            // get the important data
            $lat = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
            $long = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
            $formatted_address = isset($resp['results'][0]['formatted_address']) ? $resp['results'][0]['formatted_address'] : "";
          
            // verify if data is complete
            if($lat && $long && $formatted_address){
             
                // put the data in the array
                $data_arr = array();            
                 
                array_push(
                    $data_arr, 
                    $lat, 
                    $long, 
                    $formatted_address
                    );
                  
                return $data_arr;
                 
            }else{
                return false;
            }
             
        }else{
            echo "<strong>ERROR: {$resp['error_message']} .</strong>";
            return false;
        }
    }

    // get latitude, longitude and formatted address
    $data_arr = geocode(urlencode($_POST['address']));
 
    // if able to geocode the address
    if($data_arr){
        $latitude = $data_arr[0];
        $longitude = $data_arr[1];
        $formatted_address = $data_arr[2];
                     
    ?>
    <!-- google map will be shown here -->
    <div id="gmap_canvas">Loading map...</div>
    <div id='map-label'>Map shows approximate location.</div>
 
    <!-- JavaScript to show google map -->
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDVydhMjuw6sW5O3rrLpNXqs2i1KF9GTtw"></script>   
    <script type="text/javascript">
        function init_map() {
            var myOptions = {
                zoom: 14,
                center: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>)
            });
            infowindow = new google.maps.InfoWindow({
                content: "<?php echo $formatted_address; ?>"
            });
            google.maps.event.addListener(marker, "click", function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
 
    <?php
    }
}
?>

    <div class="container mt25">
        <div class="row">
            <div class="col-md-12">
                    <div id='address-examples'>
                        <h3>Address examples:</h3>
                        <h5>1. 4/2, Sobhanbag, Mirpur Rd, Dhaka 1207 </h5>
                        <h5>2. Nilkhet Rd, Dhaka 1000</h5>
                    </div>
            </div>
        </div>
        <div class="row">
        <form action="" method="post">
            <div class="col-md-4">
                    <input type='text' class="form-control" name='address' placeholder='Enter any Address here' />
            </div>
            <div class="col-md-4">
                <input type="submit" value="Get Geo Code" class="btn btn-success"/>
            </div>
            </form>
        </div>
        
    </div>
</body>
</html>